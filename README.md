# Activity Meter #

The Activity Meter consists of a server - client software where clients can be sensors or reception clients. The sensors send data to the server asynchronously, the server reads it and sum the incoming data of each sensor during 1 second, then it's distributed to the clients and drawn on a client UI.

### Description of the solution ###

The code consists of two main classes:

- The ActivityMeterServer class which contains two independent Sockets, one to handle Sensors connection requests and the other one to handle client connection requests. Once a sensor is connected the server generates a assistant which process the data coming to the server. When a client is connected the server creates a ClientAssistant which assists the client when the data is ready to be sent.

- The ClientUserInterface class which opens an User Interface which contains a chart and a text field to write the IP of the server. If the client connects successfully, (for testing reasons) a new virtual sensor is created and the client starts to receive data from the server. 

### Architecture ###

The software was developed in a Client/Server Architectural style. The Client/Server connection is made through a raw Socket connection via TCP-IP Protocol. The server consists of two Sockets for two types of clients which invoke a sub-assistants for each type of client (Sensor and Visualizer).

### Deployment ###

1. Install the last version of Java JRE (Or JDK if you want to open the project) downloading and following the steps from: [Java SE](http://www.oracle.com/technetwork/java/javase/downloads/index.html)

2. Since java is a multi-platform programming language, the deployment can be done via the .jar executable files because it's enough having the Java JRE installed.

### Project Downloading and Running ###

1. Install the last version of Java JRE (Or JDK if you want to open the project) downloading and following the steps from: [Java SE](http://www.oracle.com/technetwork/java/javase/downloads/index.html)
2. Install git following the steps at: [Git Installation](https://git-scm.com/book/en/v2/Getting-Started-Installing-Git)
3. Open the command line and write the following:
``` git clone https://bitbucket.org/dfonnegra/activitymeter/src/master/ ```
4. Optional Step: Open the cloned project with the eclipse IDE and run the class ActivityMeterServer. (This is better because as the server class has no UI yet, running the .jar wont show anything, everything will be a fact of faith. 
5. If you ignored the last step, run the ActivityMeterServer.jar.
6. With the server opened, the clients can connect via the ClientUserInterface.jar. When the UI is opened the client must insert the server's IP and click on "Conectar" button.
7. To test the sensor connection, the server can be accessed via telnet connection. In the command line write: ``` telnet ip port ```, Where ip is the IP address of the server and port is by default 27878. Then, if connection is successful, write random values and press enter to send them.
Note: If you have problems while running the .jar files, you can run them in the command line with the following command:

```
java -jar \path\to\jar\file
```
