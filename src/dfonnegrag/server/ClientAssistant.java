package dfonnegrag.server;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.Socket;

public class ClientAssistant{

    private PrintWriter writer;
        
    public ClientAssistant(Socket client) {
        try {
            writer = new PrintWriter(client.getOutputStream(), true);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    
    public void sendSensorValueToClient(String sensorName, double value) {
        if (writer.checkError()) {
            return;
        }
        writer.println(sensorName + " "+ value);
    }
}
