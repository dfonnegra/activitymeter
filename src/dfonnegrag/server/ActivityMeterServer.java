package dfonnegrag.server;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;

import dfonnegrag.server.sensor.SensorAssistant;

public class ActivityMeterServer {

    private ServerSocket clientServer;
    private ServerSocket sensorServer;
    private static final int CLIENT_PORT = 27877;
    private static final int SENSOR_PORT = 27878;
    private List<ClientAssistant> clientList;
    private int sensorCount = 0;
    
    public ActivityMeterServer() {
        //Starts the sockets which assist the clients and the sensors
        try {
            clientServer = new ServerSocket(CLIENT_PORT);
            sensorServer = new ServerSocket(SENSOR_PORT);            
        } catch (IOException e) {
            e.printStackTrace();
        }
        clientList = new ArrayList<ClientAssistant>();
        //Starts the threads which listen to client and sensor connection requests.
        new Thread(new Runnable() {
            @Override
            public void run() {
                while(true) {
                    try {
                        Socket sock = clientServer.accept();
                        handleClientConnection(sock);
                    } catch (IOException e) {
                        e.printStackTrace();                            
                    }
                }
            }                
        }).start();            
        new Thread(new Runnable() {
            @Override
            public void run() {
                while(true) {
                    try {
                        Socket sock = sensorServer.accept();
                        handleSensorConnection(sock);
                    } catch (IOException e) {
                        e.printStackTrace();                            
                    }
                }
            }                
        }).start();
        
    }
    
    public void handleClientConnection(Socket clientSocket) {
        ClientAssistant assistant = new ClientAssistant(clientSocket);
        clientList.add(assistant);
        System.out.println("Client succesfully connected");
    }
    
    public void handleSensorConnection(Socket sensorSocket) {
        new Thread(new SensorAssistant("Sensor "+sensorCount, sensorSocket, clientList)).start();
        System.out.println("Sensor sucessfully connected");
        sensorCount++;
    }
    
    public static void main(String[] args) {
        new ActivityMeterServer();
    }

}
