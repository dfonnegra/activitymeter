package dfonnegrag.server.sensor;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Socket;
import java.util.List;

import dfonnegrag.server.ClientAssistant;

public class SensorAssistant implements Runnable {

    private List<ClientAssistant> clientList;
    private SensorMessageAssistant messageAssistant;
    private BufferedReader inBuff;
    private String sensorName;
    
    public SensorAssistant(String sensorName, Socket sensorSocket, List<ClientAssistant> clientList) {
        this.sensorName = sensorName; 
        try {
            inBuff = new BufferedReader(new InputStreamReader(sensorSocket.getInputStream()));
        } catch (IOException e) {
            e.printStackTrace();
        }
        this.clientList = clientList;
        this.messageAssistant = new SensorMessageAssistant(sensorName, inBuff);
        new Thread(this.messageAssistant).start();
    }
    
    @Override
    public void run() {        
        while(!messageAssistant.closedBuffer()) {
            int numOfClients = clientList.size();
            double sumVal = messageAssistant.querySum();
            for(int i = 0; i < numOfClients; i++) {                        
                clientList.get(i).sendSensorValueToClient(sensorName, sumVal);   
            }
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

}
