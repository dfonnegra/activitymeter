package dfonnegrag.server.sensor;

import java.io.BufferedReader;
import java.io.IOException;


public class SensorMessageAssistant implements Runnable {

    private double sumFromLastQuery = 0;
    private BufferedReader inBuff;
    private boolean closedBuffer = false;
    private String sensorName;
    
    public SensorMessageAssistant(String sensorName, BufferedReader inBuff) {
        this.sensorName = sensorName; 
        this.inBuff = inBuff;
    }
    
    @Override
    public void run() {
        try {
            for(String line = inBuff.readLine(); line != null ; line = inBuff.readLine()) {
                try {
                    double value = Double.parseDouble(line);
                    sumFromLastQuery += value;                        
                }catch(NumberFormatException ex) {
                    break;
                }
            }
        }catch(IOException ex) {
            System.out.println(sensorName+" disconnected");
        }
        closedBuffer = true;
    }
    
    public double querySum() {
        double querySum = sumFromLastQuery;
        sumFromLastQuery = 0;
        return querySum;
    }
    
    public boolean closedBuffer() {
        return closedBuffer;
    }

}
