package dfonnegrag.client;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.Socket;

public class VirtualSensor implements Runnable {

    private Socket sensorSock;
    private PrintWriter writer;
    private static final int SENSOR_PORT = 27878;
    private final int sleepTime;
    
    public VirtualSensor(String ip) {
        try {
            sensorSock = new Socket(ip, SENSOR_PORT);
            writer = new PrintWriter(sensorSock.getOutputStream());
        } catch (IOException e) {
            e.printStackTrace();
        }
        sleepTime = (int)Math.round(90*Math.random()+10.0);
    }
    
    @Override
    public void run() {
        while(true) {
            if (writer.checkError()) break;
            writer.println(Math.random()*sleepTime/5.0);
            try {
                Thread.sleep(sleepTime);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

}
