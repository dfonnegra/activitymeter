package dfonnegrag.client;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Socket;
import java.util.HashMap;
import java.util.Map;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.chart.LineChart;
import javafx.scene.chart.XYChart;
import javafx.scene.control.TextField;
import javafx.stage.Stage;


public class ClientUserInterface extends Application implements Runnable{

    private static final int CLIENT_PORT = 27877;
    private Stage mainStage;
    private Socket clientSocket;
    private BufferedReader inBuff;
    @FXML private TextField ipTextBox;
    @FXML private LineChart<Double, Double> activeClientsChart;
    private Map<String, Integer> fromSensorNameToIndex;
    private double startingTime = 0;
   
    
    @Override
    public void start(Stage primaryStage) throws Exception {
        mainStage = primaryStage;
        Parent root = FXMLLoader.load(getClass().getResource("ClientUserInterface.fxml"));
        Scene scene = new Scene(root);

        mainStage.setOnCloseRequest(event -> System.exit(0));
        mainStage.setTitle("Cliente");
        mainStage.setResizable(false);
        mainStage.setScene(scene);
        mainStage.show();
    }

    public static void main(String[] args) {
        launch(args);
    }    
    
    @FXML protected void connectButtonClick(ActionEvent event) {
        if (clientSocket != null) return;
        try {        
            String ip = ipTextBox.getText();    
            Socket tmpSock = new Socket(ip, CLIENT_PORT);
            clientSocket = tmpSock;
            inBuff = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
            fromSensorNameToIndex = new HashMap<String, Integer>();
            startingTime = System.currentTimeMillis();
            //Platform.runLater(this);
            new Thread(this).start();
            new Thread(new VirtualSensor(ip)).start();;
        }catch(IOException io) {
            io.printStackTrace();
        }
    }

    @Override
    public void run() {
        try {
            System.out.println("Connection Stablished...");
            for (String line = inBuff.readLine(); line != null; line = inBuff.readLine()) {
                String[] answer = line.split(" ");
                String sensorName = answer[0]+" "+answer[1];
                double sensorValue = Double.parseDouble(answer[2]);
                double time = (System.currentTimeMillis()-startingTime)/1000.0;
                XYChart.Series<Double, Double> series;
                
                if (!fromSensorNameToIndex.containsKey(sensorName)) {
                    fromSensorNameToIndex.put(sensorName, fromSensorNameToIndex.size());
                    series = new XYChart.Series<Double, Double>();
                    series.setName(sensorName);
                    Platform.runLater(() -> activeClientsChart.getData().add(series));                                                    
                }else {
                    series = activeClientsChart.getData().get(fromSensorNameToIndex.get(sensorName));
                }
                Platform.runLater(() -> 
                    series.getData().add(new XYChart.Data<Double, Double>(time, sensorValue)));
                Thread.sleep(100);
            }
            
        }catch(IOException | InterruptedException ex) {
            ex.printStackTrace();
        }
    }
    
}
